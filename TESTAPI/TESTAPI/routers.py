from rest_framework import routers

from api.viewsets import *

router = routers.DefaultRouter()
# Our API
router.register(r'city', CityViewset, basename='city')
router.register(r'country', CountryViewset, basename='country')
