from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from .routers import router
from api.viewsets import *

urlpatterns = [
    # url('^admin/', admin.site.urls),
    url('^api/v1/', include(router.urls), name='api'),
    # url(r'^store/api-v1/docs/$', schema_view, name='schema-redoc'),

]
# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

