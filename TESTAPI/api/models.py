from django.db import models


class City(models.Model):
    itm_id = models.AutoField(primary_key=True)
    itm_desc = models.CharField(max_length=40, blank=True, null=True)
    itm_code = models.CharField(max_length=10, blank=True, null=True)
    itm_isactive = models.BooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'City'


class Country(models.Model):
    itm_id = models.AutoField(primary_key=True)
    itm_desc = models.CharField(max_length=40, blank=True, null=True)
    itm_code = models.CharField(max_length=10, blank=True, null=True)
    itm_isactive = models.BooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    phonecode = models.CharField(db_column='phoneCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    updated_at = models.DateTimeField(blank=True, null=True)
    item_seq = models.CharField(db_column='Item_Seq', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        # managed = False
        db_table = 'Country'