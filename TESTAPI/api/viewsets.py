from rest_framework.response import Response
from rest_framework import serializers, viewsets

from api.serializers import *


class CityViewset(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    # http_method_names = ['get']


class CountryViewset(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    # http_method_names = ['get']
